@extends('layout.master')

@section('title')
    Register Page
@endsection

@section('content')

<h1><strong>Buat Account Baru!</strong></h1>

<h2><strong>Sign Up Form</strong></h2>
<form class="generated-form" method="post" action="{{route('index.welcome')}}" target="_self">
    @csrf
    <label for="text">First Name:</label><br><br>
    <input type="text" id="text" name="firstname" required><br><br>
    <label for="text">Last Name:</label><br><br>
    <input type="text" id="text" name="lastname" required><br><br>
    <label for="text">Gender:</label><br><br>
    <input type="radio" id="gender" name="gender" value="male" required>
    <label for="male">Male</label><br>
    <input type="radio" id="gender" name="gender" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="gender" name="gender" value="other">
    <label for="other">Other</label><br><br>
    <label for="Nationality">Nationality:</label><br><br>
    <select id="Nationality" name="nationality" required>
        <option value="Indonesian">Indonesian</option>
    </select> <br><br>
    <label for="other">Language Spoken</label><br><br>
    <input type="checkbox" id="language" required name="language" value="Bike">
    <label for="vehicle1">Bahasa Indonesia</label><br>
    <input type="checkbox" id="language2" name="language2" value="Car">
    <label for="vehicle2"> English</label><br>
    <input type="checkbox" id="language3" name="language3" value="Boat">
    <label for="language3"> Other</label><br><br>
    <label for="other">BIO</label><br><br>
    <textarea id="w3codeGenText" required name="bio" rows="3" cols="50"></textarea><br><br>
    <input type="submit" value="Submit"><br><br>
</form>

@endsection