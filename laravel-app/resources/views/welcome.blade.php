@extends('layout.master')

@section('title')
    Welcome Page
@endsection

@section('content')

<h1><strong>SELAMAT DATANG, {{$firstname." ".$lastname}}!</strong></h1>

<h2><strong>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</strong></h2>
@endsection