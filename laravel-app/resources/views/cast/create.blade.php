@extends('layout.master')

@section('title')
    Create Cash 
@endsection

@section('content')
    <h1>Tambah Data Pemain Film Baru</h1>

    <form action="{{ route('cast.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="nama">Nama:</label>
            <input type="text" name="nama" id="nama" class="form-control">
        </div>
        <div class="form-group">
            <label for="umur">Umur:</label>
            <input type="number" name="umur" id="umur" class="form-control">
        </div>
        <div class="form-group">
            <label for="bio">Bio:</label>
            <textarea name="bio" id="bio" class="form-control"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
