@extends('layout.master')

@section('title')
    Show Cash 
@endsection

@section('content')
    <h1>Detail Data Pemain Film</h1>
    <table class="table">
        <tr>
            <th>Nama:</th>
            <td>{{ $cast->nama }}</td>
        </tr>
        <tr>
            <th>Umur:</th>
            <td>{{ $cast->umur }}</td>
        </tr>
        <tr>
            <th>Bio:</th>
            <td>{{ $cast->bio }}</td>
        </tr>
    </table>
    <a href="{{ route('cast.edit', $cast->id) }}" class="btn btn-success">Edit</a>
    <form action="{{ route('cast.destroy', $cast->id) }}" method="POST" class="d-inline">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">Hapus</button>
    </form>
@endsection
