@extends('layout.master')

@section('title')
    Edit Cash 
@endsection



@section('content')
    <h1>Edit Data Pemain Film</h1>

    <form action="{{ route('cast.update', $cast->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama:</label>
            <input type="text" name="nama" id="nama" class="form-control" value="{{ $cast->nama }}">
        </div>
        <div class="form-group">
            <label for="umur">Umur:</label>
            <input type="number" name="umur" id="umur" class="form-control" value="{{ $cast->umur }}">
        </div>
        <div class="form-group">
            <label for="bio">Bio:</label>
            <textarea name="bio" id="bio" class="form-control">{{ $cast->bio }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
