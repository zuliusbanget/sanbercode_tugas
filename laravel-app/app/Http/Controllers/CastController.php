<?php

namespace App\Http\Controllers;

use App\Models\Casts;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index()
    {
        $casts = Casts::all();
        return view('cast.index', compact('casts'));
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);

        Casts::create($validatedData);

        return redirect('/cast')->with('success', 'Data pemain film berhasil disimpan.');
    }

    public function show($cast_id)
    {
        $cast = Casts::findOrFail($cast_id);
        return view('cast.show', compact('cast'));
    }

    public function edit($cast_id)
    {
        $cast = Casts::findOrFail($cast_id);
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $cast_id)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);

        $cast = Casts::findOrFail($cast_id);
        $cast->update($validatedData);

        return redirect('/cast')->with('success', 'Data pemain film berhasil diperbarui.');
    }

    public function destroy($cast_id)
    {
        $cast = Casts::findOrFail($cast_id);
        $cast->delete();

        return redirect('/cast')->with('success', 'Data pemain film berhasil dihapus.');
    }
}
