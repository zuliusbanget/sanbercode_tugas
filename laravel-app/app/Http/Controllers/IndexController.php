<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class IndexController extends Controller
{
    public function utama()
    {
        return view('utama');
    }

    public function register(){
        return view('register');
    }

    public function master(){
        return view('layout.master');
    }

    public function datatables(){
        return view('datatables');
    }
    
    public function welcome(Request $request){
        $firstname = Str::upper($request->firstname);
        $lastname = Str::upper($request->lastname);
        $gender = $request->gender;
        $nationality = $request->nationality;
        $language = $request->language;
        $language2 = $request->language2;
        $language3 = $request->language3;
        $bio = $request->bio;
        return view('welcome',['firstname'=>$firstname,'lastname'=>$lastname]);
    }

    


}
