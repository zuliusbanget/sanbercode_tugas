CREATE TABLE users (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    NAME VARCHAR(255),
    email VARCHAR(255),
    PASSWORD VARCHAR(255)
);

CREATE TABLE categories (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    NAME VARCHAR(255)
);


CREATE TABLE books (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255),
    summary TEXT,
    stock INTEGER,
    category_id INTEGER,
    FOREIGN KEY (category_id) REFERENCES categories(id)
);
