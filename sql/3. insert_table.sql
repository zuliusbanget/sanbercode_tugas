INSERT INTO users (NAME, email, PASSWORD) VALUES 
    ('John Doe', 'john@doe.com', 'john123'),
    ('Jane Doe', 'jane@doe.com', 'jenita123');


INSERT INTO categories (NAME) VALUES 
    ('Novel'),
    ('Manga'),
    ('Comic'),
    ('History'),
    ('IT');


INSERT INTO books (title, summary, stock, category_id) VALUES 
    ('One piece', 'The series focuses on Monkey D Luffy, a young man made of rubber', 50, 2),
    ('Laskar Pelangi', 'Belitung is known for its richness in tin, making it one of Indonesia''s richest islands', 25, 1),
    ('Your Name', 'Mitsuha Miyamizu, a high school girl living in the fictional town of Itomori in Gifu Prefecture''s', 15, 2);
