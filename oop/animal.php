<?php

class Animal {
    public $name;
    public $cold_blooded = "no";

    public function __construct($name) {
        $this->name = $name;
    }

    public function get_name() {
        return "Name : " .$this->name;
    }

    public function get_legs() {

        if ($this->name=="kera sakti"){
            return "Legs : 2";
        } else {
            return "Legs : 4";
        }
         
    }

    public function get_cold_blooded() {
        return "cold blooded :".$this->cold_blooded;
    }
}

?>
