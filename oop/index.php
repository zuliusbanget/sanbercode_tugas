<?php

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');


$sheep = new Animal("shaun");

echo $sheep->get_name(); // "shaun"
echo "<br>";
echo $sheep->get_legs(); // 2
echo "<br>";
echo $sheep->get_cold_blooded(); // false
echo "<br>";
echo "<br>";

$sheep = new Animal("buduk");
$kodok = new Frog("buduk");
echo $sheep->get_name(); 
echo "<br>";
echo $sheep->get_legs(); 
echo "<br>";
echo $sheep->get_cold_blooded(); 
echo "<br>";
echo $kodok->jump(); 
echo "<br>";
echo "<br>";

$sheep = new Animal("kera sakti");
$sungokong = new Ape("kera sakti");
echo $sheep->get_name(); 
echo "<br>";
echo $sheep->get_legs(); 
echo "<br>";
echo $sheep->get_cold_blooded(); // false
echo "<br>";
echo $sungokong->yell();
?>
